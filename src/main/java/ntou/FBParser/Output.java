package ntou.FBParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class Output {
	private final HSSFWorkbook workbook;
	private final HSSFSheet[] sheet;

	public Output() {
		workbook = new HSSFWorkbook();
		sheet = new HSSFSheet[10];
		for(int i = 1; i <= 10; i++){
			sheet[i-1] =	workbook.createSheet("Group"+i);
		} 
	}

	public void insert(int i, ArrayList<ArrayList<String>> sources) {
		int rownum = 0;
		for (ArrayList<String> source : sources) {
			Row row = sheet[i].createRow(rownum++);
			int cellnum = 0;
			for (String item : source) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellValue(item);
			}
		}
	}

	public void wirteFile() {
		try {
		    FileOutputStream out = new FileOutputStream(new File("D:\\fb\\FB.xls"));
		    workbook.write(out);
		    out.close();
		    System.out.println("Excel written successfully..");
		     
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
}
